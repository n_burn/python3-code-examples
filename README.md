# README #

The code in this repository is intended to provide "short enough to understand" example uses of
various Python modules, functions, and classes.

### Notes ###

* Please do not remove the prefix ("eg_") from code files as this may cause unexpected behaviour.

### Licesnse ###

Files in this repository are releases under the license specified at the beginning of the file.
Files that do not contain a license are released under the ISC license listed below.

### ISC Licesnse Text###

Permission to use, copy, modify, and/or distribute this software for any purpose with or 
without fee is hereby granted, provided that the above copyright notice and this permission 
notice appear in all copies.

THE SOFTWARE IS PROVIDED “AS IS” AND ISC DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE 
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE 
FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING 
FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER 
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

http://www.isc.org/downloads/software-support-policy/isc-license/

https://en.wikipedia.org/wiki/ISC_license
