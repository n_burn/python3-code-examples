# for.py
# for is used to create loops of a known count from an iterative object*
# * e.g. lists, strings, tuples, etc
# for VARIABLE in OBJECT:
# the variable will be assigned the next element in the given object
# starting from the first element and proceeding to the last


# run through a single iteration, using a 1 element list
for x in [4]:
    print(x)
print()  # linebreak

# run through 3 iterations, using a 3 element list
for x in [4, 9, 4]:
    print(x)
print()  # linebreak

# run through 3 iterations, using the 3 elements in the list
for x in ["foo", "bar", "baz"]: 
    print(x)
print()  # linebreak

# run through 4 iterations, using the 4 elements in the string
for x in "town": 
    print(x)
print()  # linebreak

# run through 3 iterations, using the range function
for x in range(3): # generates a list of [0, 1, 2]
    print(x)
print()  # linebreak

# run through 3 iterations, using the range function
for x in range(2, 5): # generates a list of [2, 3, 4]
    print(x)
print()  # linebreak

# range can also be stored as a variable to use for later iteration
y = range(2, 6)
print(y)  # prints: range(2,6)
for x in y: 
    print(x)  # prints: 2, 3, 4, 5
print()  # linebreak

# run through 3 iterations, using the range function
for x in range(1, 10, 2):  # generates a list of [1, 3, 5, 7, 9]
    print(x)
print()  # linebreak


foo=['a', 'b', 'c', 'd', 'e', 'f']
fooLen = len(foo)

# forward 'a' through 'f'
for x in foo:
    print(x)
print()

# forward 'a' through 'f'
for x in range(fooLen):
    print(foo[x]) 
print()

# backward 'f' through 'a'
for x in range(fooLen-1, -1, -1):
    print(foo[x]) 
print()

# backward 'f' through 'a'
for x in range(1, fooLen+1):
    print(foo[-x]) 
print()

# break, exits for loop immediately
for i in [1, 3, 7, 4, 5, 2]:
    # if i greater than 4, exit the for loop
    if i > 4:
        break
    print(i)
    # prints  1  3

for i in ['a', 3, 'c', 4, 5, 't']:
    # if i is int, exit for loop
    if type(i) is int:
        break
    print(i)
    # prints  a

# continue, ends current iteration and moves to next element in loop
for i in [1, 3, 7, 4, 5, 2]:
    # if i is greater than 4, move to next item in list
    if i > 4:
        continue
    print(i)
    # prints  1  3  4  2

for i in [2.3, 3, 1.0, 4, 5, 9.2]:
    # if i is int, move to next item in list
    if type(i) is int:
        continue
    print(i)
    # prints  2.3  1.0  9.2
