# eg_print.py
# print: built in print function

# this produce an error due to missing parentheses (required in python 3)
# print 9

# print "9" on its own line
print(9)

# print an empty line
print()

# prints the string "hello" followed by a new line
print("hello")

# prints concatenated string "hitop" followed by a new line
print("hi" + "top")

# prints concatenated string "hi top" followed by a new line
print("hi", "top")

# prints string "hihihi" followed by a new line
print("hi" * 3)

# prints string "hi", but with a space instead of a line break
print("hi", end=" ")

# prints string "hello" to demonstrate the above end function does not last
print("hello")

# printing strings and float values together
x = 2/3
print("2 divided by 3 equals " + str(x))

# printing the same result as the above print statement
print("2 divided by 3 equals", 2/3)
