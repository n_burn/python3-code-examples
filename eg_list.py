# test_list.py
# a list is mutable (changeable) sequence of data

# make an empty list
empty_list1 = []
empty_list2 = list()
print("empty_list1:", empty_list1)  # prints []
print("empty_list2:", empty_list2)  # prints []
print()  # line break

# store a list of string elements in the variable mr_list
mr_list = ["I", "am", "a", "list"]

# print out text stored in mr_list followed by a newline character
print("mr_list:", mr_list, "\n")  # prints: ['I', 'am', 'a', 'list']

# get the length / size of a list
print("len(mr_list):", len(mr_list), '\n')  # prints 4

# print out the individual string elements of mr_list located at the given position
print("mr_list[0]:", mr_list[0])  # I
print("mr_list[1]:", mr_list[1])  # am
print("mr_list[2]:", mr_list[2])  # a
print("mr_list[3]:", mr_list[3])  # list
print()  # line break

# see if an element is in a list with "in"
# eg:  1 in [1, 2, 3]  True  ;  0 in [1, 2, 3]  False
print("'I' in mr_list", 'I' in mr_list)  # True

# see if an element is in a list then
# find the index (position) of that element
# note "index" should always be run after testing with "in" to prevent errors
if "am" in mr_list:
    print( 'mr_list.index("am"):', mr_list.index("am") )  # 1

# replace one element of a list with another
mr_list[0] = "This"
mr_list[1] = "is"

# append / add data to an element
mr_list[1] = "almost " + mr_list[1]

# print out the changed mr_list
print("changed mr_list:", mr_list,'\n')  # ['This', 'almost is', 'a', 'list']

# adding 2 lists
L1 = [1, 2, 3]
L2 = [5, 6]
L3 = L1 + L2
print("L3:", L3, '\n') # [1, 2, 3, 5, 6]

# multiplying a list
L2 = L2 * 3
L4 = [3] * 5
print("L2:", L2)  # [5, 6, 5, 6, 5, 6]
print("L4:", L4,'\n')  # [3, 3, 3, 3, 3]

# creating lists of lists (a list of 3 lists, each containing 3 elements)
L1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

# creating lists of lists
# two lists being combined do not need the same number of elements
L1 = [1, 2, 3]
L2 = [5, 6, 7, 8]
L3 = [L1, L2]
print("L3:", L3, '\n')  # [[1, 2, 3], [5, 6, 7, 8]]

# accessing individual elements in a list of lists
# 3rd element (2) of 1st list (0)
print("L3[0][2]:", L3[0][2], '\n')  # 3

# access 2nd element from end of last list (list -1)
print("L3[-1][-2]:", L3[-1][-2], '\n')  # 7

# the list function can be used to create an empty list
# when given no arguments
print( "list():", list() )  # []

# list can take an "iterable object" as an argument
# e.g.: something with elements that can be indexed with square brackets
# strings, tuples, lists, etc. eg: mrString[1]
print( 'list("cats"):', list("cats") )  # ['c', 'a', 't', 's']

# list has a sub-function, range, that makes incremented lists
# range can take 1, 2, or 3 parameters
# list(range(PARAMETERS))

# with one parameter, range creates a positively incremented list
# range starts at zero and stops before the the supplied value
# incrementing by a value of 1
# if range is supplied a negative number, an empty list is created
print( "list(range(4)):", list(range(4)) )  # [0, 1, 2, 3]

# with two parameters you can specify range's starting value
# list(range(START_AT, STOP_BEFORE)
# if the 2nd value is less than the first, an empty list is created
print( "list(range(2, 7)):", list(range(2, 7)) )  # [2, 3, 4, 5, 6]

# with three parameters you can specify the amount range increments
# list(range(START_AT, STOP_BEFORE, INCREMENTS_OF))
# the increment value can be negative
print( "list(range(2, -10, -3)):", list(range(2, -10, -3)) )  # [2, -1, -4, -7]
print()

# display largest and smallest items in mr_list:
numList = list(range(10))
print("numList:", numList)  # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
print("max(numList):", max(numList))  # 9
print("min(numList):", min(numList), '\n')  # 0

# display the length of mr_list:
print("len(numList):", len(numList), '\n')  # 10

# Slicing a list, used to only access part of a list
# LISTNAME[START_AT : STOP_BEFORE]
# LISTNAME[START_AT :]  # goes to last element
# LISTNAME[: STOP_BEFORE]  # goes from first element to last before the stop position
# slice positions use array indexing (starting from 0)
print("numList[2:5] :", numList[2:5])  # [2, 3, 4]
print("numList[4:]  :", numList[4:])  # [4, 5, 6, 7, 8, 9]
print("numList[:4]  :", numList[:4], '\n')  # [0, 1, 2, 3]

# remove a specific item from a list
del(numList[0]) # remove element at position 0
print("post del(numList[0]):", numList, '\n')  # [1, 2, 3, 4, 5, 6, 7, 8, 9]

# remove the 1st occurrence of value in parentheses
# numList.remove(1) -> would remove 1 from numList
# as with the "index" method, "remove" should be used in conjunction
# with an "in" test to avoid errors
if 5 in numList:
    numList.remove(5)
print("numList after remove():", numList, '\n')  # [1, 2, 3, 4, 6, 7, 8, 9]

# remove the last item from a list and return the removed item
print("numList.pop():", numList.pop())  # print shows return value, 9
print("numList after pop():", numList)  # [1, 2, 3, 4, 6, 7, 8]
# pop can also be used to remove a particular element
print("numList.pop(5):", numList.pop(5)) # removes element at position 5 (7)
print("numList:", numList,'\n')  # [1, 2, 3, 4, 6, 8]

# add a single element to the end of a list
numList.append(9) # add 9 to the end
numList.append([10]) # add 9 to the end
print("numList after append():", numList,'\n')  # [1, 2, 3, 4, 6, 8, 9]

# add an list (or another iterable type) to the end of a list
numList = [1]  # reducing size for simplicity
numList.extend([3]) # add 3 to the end of numList
numList.extend([2, 4]) # add 2 and 4 to the end
print("numList after extend():", numList,'\n')  # [1, 3, 2, 4]
numList2 = [8, 6]
numList.extend(numList2) # add contents of numList2 to end of numList
print("numList after extend():", numList,'\n')  # [1, 2, 3, 4, 8, 6]

# reverse the order of numList
numList.reverse()
print("numList after reverse():", numList,'\n')  # [6, 8, 4, 3, 2, 1]

# sort numList least to greatest (ascending)
numList.sort()  # [1, 2, 3, 4, 6, 8]
#numList.sort()
print("numList after sort():", numList)
# sort numList greatest to least (descending)
numList.sort(reverse=True)
print("numList after reverse sort:", numList, '\n')  # [8, 6, 4, 3, 2, 1]

# count the number of times a value occurs in a list
print("numList.count(3):", numList.count(3))  # 1

# insert a new element at a particular position
# LISTNAME.insert(WHERE_TO_INSERT, VALUE_TO_INSERT)
# ls = [1, 3, 4]  ls.insert(0, 10)  print(ls) 10, 1, 3, 4
numList.insert(3, 5)
print("numList after insert:", numList, '\n')  # [8, 6, 4, 5, 3, 2, 1]

# ================================================================
# list comprehension
# a compact way of creating a list using a for loop
# ================================================================

# Example 1
h1 = [h for h in range(5)]
print("h1:", h1)  # [0, 1, 2, 3, 4]

# Example 2
i1 = [i for i in range(5) if i != 2 ]
print("i1:", i1)  # [0, 1, 3, 4]


# Example 3
j1 = [j/10 for j in range(5)]
print("j1:", j1)  # [0.0, 0.1, 0.2, 0.3, 0.4]

# the above is roughly the same as:
k1 = []
for k2 in range(5):
    k1.append(k2/10)
print("k1:", k1)


# Example 4
# list comprehension can be combined 
n1 = [n for m in range(5) for n in range(m//2)]
print("n1:", n1)  # [0, 0, 0, 1]

# the above is roughly the same as:
o1 = []
for o in range(5):
    for p in range(o//2):
        o1.append(p)
print("o1:", o1)
